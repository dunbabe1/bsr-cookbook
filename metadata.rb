name             "bsr"
maintainer       "Novartis"
maintainer_email "devops@novartis.com"
license          "All rights reserved"
description      "Installs/Configures Gethelp"
version          "0.0.5"

depends "ci"
depends "mw-jboss6"
