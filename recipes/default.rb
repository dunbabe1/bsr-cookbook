ENV['http_proxy'] = "http://usca-proxy01.na.novartis.net:2011"
ENV['https_proxy'] = "http://usca-proxy01.na.novartis.net:2011"

node.override['java']['jdk_version'] = '6'
node.override['java']['install_flavor'] = 'openjdk'

include_recipe "java"
include_recipe "ci"
include_recipe "mw-jboss6"


case deploy_env
when nil, ''
  env='dev'
when 'release'
  env='prod'
when 'test'
  env='test'
when 'prod'
  env='prod'
when 'ci'
  env='dev'
else
  env='dev'
end

version=node["project_info"]["pinpromote"]["version"]
pullFrom=""

if version.to_s.strip.length == 0
  pullFrom="lastSuccessfulBuild"
else
  pullFrom="#{version}"
end


execute "/bin/rm -rf /apps/jars"
## Remove existing bsr-solr-bin.zip file
execute "/bin/rm -rf /apps/bsr-solr-bin.zip"
execute "bin/rm -rf /apps/bsr-solr"

directory "/apps/jars" do
  recursive true
  user node['jboss']['user']
  group node['jboss']['group']
  mode '0755'
  action :create
end

remote_file "/apps/jars/downloadP0l4h094504.zip" do
  source "http://cid-archive.nibr.novartis.net/bsr/downloadP0l4h094504.zip"
  action :create
end

bash "unzip" do
  cwd "/apps/jars"
  user "root"
  code <<-EOH
    unzip ./downloadP0l4h094504.zip
    EOH
end

%w{jars}.each do |dir|
  execute "chmod_#{dir}" do
    cwd '/apps/'
    command "chmod -R 755 #{dir}"
  end
end

jbossConf = ["jboss-logging.xml","mail-service.xml"]
jbossConf.each do |conf_file|
  template "#{node['jboss']['jboss_home']}/server/default/deploy/#{conf_file}" do
    source "#{conf_file}.erb"
    user node['jboss']['user']
    group node['jboss']['group']
    mode 0664
    action :create
  end
end

template "#{node['jboss']['jboss_home']}/server/default/conf/login-config.xml" do
  source "login-config.xml.erb"
  user node['jboss']['user']
  group node['jboss']['group']
  mode 0664
  action :create
 end

template "#{node['jboss']['jboss_home']}/server/default/deploy/bsr-xa-ds.xml" do
  source "bsr-xa-ds.#{env}.xml.erb"
  user node['jboss']['user']
  group node['jboss']['group']
  mode 0664
  action :create
 end

template "#{node['jboss']['jboss_home']}/bin/run.conf" do
  source "run.conf.erb"
  user node['jboss']['user']
  group node['jboss']['group']
  mode 0755
 end

template "#{node['jboss']['jboss_home']}/start_bsr.sh" do
  source "jboss_start-BSR.#{env}.sh.erb"
  user node['jboss']['user']
  group node['jboss']['group']
  mode 0755
  variables(
    :SYS_BSR_USER => data_bag_item('projects', 'bsr')['username'],
    :SYS_BSR_PASSWORD => data_bag_item('projects', 'bsr')['password']
  )
 end

 directory "/apps/jboss/server/default/log" do
  owner "#{node['jboss']['user']}"
  group "#{node['jboss']['group']}"
  recursive true
  mode 0755
  action :create
 end

 directory "/apps/jboss/log" do
  owner "#{node['jboss']['user']}"
  group "#{node['jboss']['group']}"
  recursive true
  mode 0755
  action :create
 end


deployments = ["bsr-ejb.war", "bsr-ui-reg.war", "bsr-solr.war"]

deployments.each do |deployment_file|
  remote_file "#{node['jboss']['jboss_home']}/server/default/deploy/#{deployment_file}" do
    user "root"
    source "http://cid-archive.nibr.novartis.net/bsr/#{pullFrom}/#{deployment_file}"
    user node['jboss']['user']
    group node['jboss']['group']
    mode 0664
    action :create
  end
end

bash "replace_jars" do
  cwd "/apps"
  user node['jboss']['user']
  code <<-EOH
    ./jars/01-replace-jars.sh
    EOH
end

remote_file "/apps/bsr-solr-bin.zip" do
  source "http://cid-archive.nibr.novartis.net/bsr/#{pullFrom}/bsr-solr-bin.zip"
  mode 0755
  action :create
  notifies :run, "bash[unzip_solr-bin]", :immediately
end

bash "unzip_solr-bin" do
  cwd "/apps"
  code <<-EOH
    unzip ./bsr-solr-bin.zip
    chown -R sys_nibr_jboss:sys_nibr_jboss /apps/bsr-solr
  EOH
  action :nothing
end

#that doesn't seem to actually stop the service
service "jboss" do
  service_name "jboss"
  action :stop
end
#this is ugly but it should work
#first get PID of jboss and then send it the stop command
bash "jboss shutdown" do
  user "root"
  code <<-EOH
  JBPID=`pgrep -f "[j]ava -server"`
  kill -9 $JBPID
  sleep 20
EOH
  only_if('pgrep -f "[j]ava -server" | grep "[0-9]" -ci')
end

#after setting it all up, let the jBoss run free if isn't already
execute "startJboss for BSR" do
  user node['jboss']['user']
  command "/apps/jboss/start_bsr.sh > /apps/jboss/server/default/log/start_bsr_`date +\%Y\%m\%d\%H\%M`.log"
  not_if 'pgrep -f "[j]ava -server" | grep "[0-9]" -ci' # -ci should make grep return 1 if true 0 if false
end
