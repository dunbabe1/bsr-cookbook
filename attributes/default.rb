override['jboss']['url'] = "http://cid-archive.nibr.novartis.net/jboss/jboss-as-distribution-6.0.0.Final.zip"
override['jboss']['version'] = "jboss-6.0.0.Final"
override['jboss']['user'] = "sys_nibr_jboss"
override['jboss']['group'] = "sys_nibr_jboss"
override['jboss']['jboss_user_home'] = node['jboss']['jboss_home']
